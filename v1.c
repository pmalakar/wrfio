/*
 * Developed by Preeti Malakar
 * Argonne National Laboratory
 * 
 * December 2014
 *
 * Collective read of variables from WRF boundary data input file 'wrfbdy_d0*' 
 *
 */

#include "common.h"

/*
 *  Read variable names
 */ 

int readVarNames (void) {

	int i=-1;

	//MPI_File_open
	FILE *fp = fopen (VAR_FNAME, "r");
	if (!fp) {
		printf("File open error\n");
		return 1;
	}

	//Read the variable list
	while (fscanf(fp, "%s", fields[++i]) == 1) ; 

	//MPI_File_close
	fclose(fp);

}

#ifdef BGQ
void getMemStats(int i) {

	Kernel_GetMemorySize( KERNEL_MEMSIZE_HEAPMAX, &heapmax[i]); 
	Kernel_GetMemorySize( KERNEL_MEMSIZE_ESTHEAPAVAIL, &estheapmax[i]); 
	Kernel_GetMemorySize( KERNEL_MEMSIZE_STACK, &stack[i]);
	Kernel_GetMemorySize( KERNEL_MEMSIZE_STACKAVAIL, &stackavail[i]);
	Kernel_GetMemorySize( KERNEL_MEMSIZE_HEAP, &heap[i]);
	Kernel_GetMemorySize( KERNEL_MEMSIZE_HEAPAVAIL, &heapavail[i]);

}
#endif

//Memory order, chunk per process
int getStart(char memOrder, int count) {

	int start;
	if (memOrder == 'X')
		start = myrank/px * count; 
	else
		start = myrank%px * count;

	return start;
}

int setupVar(int var, int varnum) {

		int i;

		if (strstr(fields[varnum], "XE") || strstr(fields[varnum], "XS")) 
			memOrder[varnum] = 'X', procs = py; 
		else 
			memOrder[varnum] = 'Y', procs = px; 

		ncid[varnum] = ncid_g;

		/* Get the varid from the variable name */
		if ((retval = ncmpi_inq_varid(ncid[varnum], fields[varnum], &varid[var]))) {
			printf("%d: ncmpi_inq_varid failed: %d\n", myrank, retval);
			ERR(retval);
		}

		/* Get the number of dimensions of this variable */
		if ((retval = ncmpi_inq_varndims(ncid[varnum], varid[var], &ndims) )) {
			printf("ncmpi_inq_varndims failed: %d\n", retval);
			ERR(retval);
		}

		/* Get the dimension ids of this variable */
		int *dimids = (int *)malloc (ndims*sizeof(int));
		if ((retval = ncmpi_inq_vardimid(ncid[varnum], varid[var], dimids) )) {
			printf("ncmpi_inq_var failed: %d\n", retval);
			ERR(retval);
		}

		start[var] = (MPI_Offset *) malloc (ndims*sizeof(MPI_Offset));
		count[var] = (MPI_Offset *) malloc (ndims*sizeof(MPI_Offset));
		dimlen = (MPI_Offset *) malloc (ndims*sizeof(MPI_Offset));

		/* Get the dimension lengths of this variable */
		for (i=0; i<ndims; i++) {
			if (retval = ncmpi_inq_dimlen (ncid[varnum], dimids[i], &dimlen[i])) {
				printf("ncmpi_inq_dimlen failed: %d\n", retval);
				ERR(retval);
			}
			start[var][i] = 0, count[var][i] = dimlen[i];
		}

		count[var][0] = 1;		//fixme: hack to get 1 timestep only	//testing purpose
	//TODO
	//	http://www2.mmm.ucar.edu/people/wrfhelp/wrfvar/code/trunk/task_for_point.html is the wrf strategy for indivisible #procs	

		count[var][ndims-1] /= procs;
		start[var][ndims-1] = getStart(memOrder[varnum], count[var][ndims-1]);	//last dimension is divided 

		size[var] = 1;
		// Data size chunk read by each file
		for (i=0; i<ndims ; i++) {
			size[var] *= count[var][i]; 
		}

		dataIn[varnum] = (float *) malloc (size[var] * sizeof(float));

		if (VERSION == 2) {
			datasets[var] = (float *) malloc (size[var] * sizeof(float));
			bufcounts[var] = size[var];
			datatypes[var] = MPI_FLOAT;
		}

		free (dimids);
		free (dimlen);

}

int readVar(int var, int varnum) {
	int i;

	if (NONBLOCKING == 1) {
		if (retval = ncmpi_iget_vara_float (ncid[varnum], varid[var], start[var], count[var], dataIn[varnum], &request[var])) 
		{ 
			printf("ncmpi_iget_vara_float failed: %d\n", retval);
			ERR(retval);
		}
	}
	else {
		if (COLL == 0) {
			if (retval=ncmpi_begin_indep_data(ncid[varnum])) {
				printf("ncmpi_begin_indep_data failed: %d\n", retval);
				ERR(retval);
			}
			if ((retval = ncmpi_get_vara_float (ncid[varnum], varid[var], start[var], count[var], dataIn[varnum]))) {
				printf("ncmpi_get_vara_float failed: %d\n", retval);
				ERR(retval);
			}
			if (retval=ncmpi_end_indep_data(ncid[varnum])) {
				printf("ncmpi_end_indep_data failed: %d\n", retval);
				ERR(retval);
			}
		}
		else {
			if ((retval = ncmpi_get_vara_float_all (ncid[varnum], varid[var], start[var], count[var], dataIn[varnum]))) {
				printf("ncmpi_get_vara_float_all failed: %d\n", retval);
				ERR(retval);
			}
		}
	}
}

/* Deallocate temporary buffers */
int freeVar(int var) {
	free(start[var]);	
	free(count[var]);	
}

/* Allocate buffers */

void allocate(void) {

		varid = (int *)  malloc (BATCHSIZE * sizeof(int));
		size = (int *)  malloc (BATCHSIZE * sizeof(int));
		start = (MPI_Offset **) malloc (BATCHSIZE * sizeof(MPI_Offset *));
		count = (MPI_Offset **) malloc (BATCHSIZE * sizeof(MPI_Offset *));
		request = (int *) malloc (BATCHSIZE * sizeof(int));
		status = (int *) malloc (BATCHSIZE * sizeof(int));

		ncid = (int *)  malloc (NUMVARS * sizeof(int));
		memOrder = (char *) malloc (NUMVARS * sizeof(char));
		dataIn = (float **) malloc (NUMVARS * sizeof(float *));

		if (VERSION == 2) {

				datasets = (float **) malloc (BATCHSIZE * sizeof(float *));
				bufcounts = (MPI_Offset *) malloc (BATCHSIZE * sizeof(MPI_Offset));
				datatypes = (MPI_Datatype *) malloc (BATCHSIZE * sizeof(MPI_Datatype));

		}

}



int main(int argc, char **argv) {

	//local variables
	int i, batch, var, gvarnum;

	MPI_Init (&argc, &argv);

	MPI_Comm_rank (MPI_COMM_WORLD, &myrank);	
	MPI_Comm_size (MPI_COMM_WORLD, &commsize);

	//USAGE: ./wrf_io BLOCKING=0/NONBLOCKING=1 BATCHSIZE>0 VERSION=0/1/2 COLL=0/1

	if (atoi(argv[1]) > 0 && argc>1) NONBLOCKING = atoi(argv[1]);
	else NONBLOCKING = 1;

	if (atoi(argv[2]) > 0 && argc>2) BATCHSIZE = atoi(argv[2]);
	else BATCHSIZE = NUMVARS;

	//VERSION = 0 - default WRF, 1 - with pnetcdf hint (refer 2011 paper), 2 - mget 
	if (argc>3) VERSION = atoi(argv[3]);
	else VERSION = 0;	//default

	//COLL = 0 - independent, 1 - collective I/O
	if (argc>4) COLL = atoi(argv[4]);
	else COLL = 1;		//default

	//Following combinations are invalid/no api, so exit
    if (VERSION == 1 && COLL == 0) return -1; 
    if (NONBLOCKING == 1 && COLL == 0) return -1; 
    if (NONBLOCKING == 0 && VERSION == 1) return -1; 

	px = mpaspect(commsize);
	py = (int)(commsize/px);

#ifdef BGQ
	MPIX_Hardware(&hw);
	ppn = hw.ppn;	
	core = hw.coreID;	
#elif CRAY
	ppn = 24;
	core = myrank%ppn;              //small hack till i figure out mpix equivalent on cray TODO     
#endif

	nodenum = commsize/ppn;

	/* Open the file */
	MPI_Info info = MPI_INFO_NULL;
	if (VERSION == 1) {
		if (MPI_Info_create (&info) == MPI_SUCCESS) {
			MPI_Info_set (info, "multi-variable I/O", "enable");
			MPI_Info_set (info, "cb_buffer_size", "33554432");
		}
	}

	tStart = MPI_Wtime();
	if ((retval = ncmpi_open(MPI_COMM_WORLD, filename, NC_NOWRITE, info, &ncid_g))) {
		printf("ncmpi_open failed: %d\n", retval);
		ERR(retval);
	}
	tEnd = MPI_Wtime() - tStart;
	MPI_Reduce(&tEnd, &max, 1, MPI_DOUBLE, MPI_MAX, root, MPI_COMM_WORLD);
	openT = max;

	/* Inquire number of variables */
	if (retval = ncmpi_inq_nvars(ncid_g, &nvarsp)) {
		printf("ncmpi_open failed: %d\n", retval);
		ERR(retval);
	}
	NUMVARS = nvarsp - 3;		//Ignoring first 3 variables: Times, md___thisbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_, md___nextbdytimee_x_t_d_o_m_a_i_n_m_e_t_a_data_

	fields = (char **) malloc (NUMVARS * sizeof (char *));
	for (i=0; i<NUMVARS; i++) fields[i] = (char *) malloc (VARNAMESIZE * sizeof(char));

	// Read variable names
	tRead = MPI_Wtime();
	readVarNames();
	tRead = MPI_Wtime() - tRead;

#ifdef DEBUG
	MPI_Reduce(&tRead, &max, 1, MPI_DOUBLE, MPI_MAX, root, MPI_COMM_WORLD);
	if (myrank == root) {
		printf("time to read %d variables %lf\n", NUMVARS, max);
	}
	printf("%d: of %d: -- %d X %d NONBLOCKING=%d VERSION=%d COLL=%d\n", myrank, commsize, px, py, NONBLOCKING, VERSION, COLL);
#endif

	allocate();

	currentBatchSize = BATCHSIZE;
		numBatches = ceil ((double)NUMVARS/BATCHSIZE);

		//Process variables in batches
		for (batch=0; batch<numBatches; batch=batch+1) {

				gvarnum = batch*BATCHSIZE;

				if (batch == numBatches-1 && NUMVARS%BATCHSIZE != 0)
						currentBatchSize = NUMVARS%BATCHSIZE;

				//begin variable inquiry
				for (var=0 ; var<currentBatchSize; var++)  
						setupVar(var, gvarnum+var);
				//end variable inquiry

				tStart = MPI_Wtime ();

				//begin variable read
				if (VERSION == 2) {
					ncmpi_mget_vara_all (ncid_g, currentBatchSize, varid, start, count, (void **) datasets, bufcounts, datatypes);
				}
				else {
					for (var=0 ; var<currentBatchSize; var++)  
						readVar(var, gvarnum+var);
					if (NONBLOCKING == 1) 
						ncmpi_wait_all (ncid_g, BATCHSIZE, request, status);
				}
				//end variable read

				tEnd = MPI_Wtime () - tStart;
				MPI_Reduce(&tEnd, &max, 1, MPI_DOUBLE, MPI_MAX, root, MPI_COMM_WORLD);

				if (retval = ncmpi_inq_get_size (ncid_g, &getSize))	ERR(retval);
				readAmount = (double)getSize/oneMB;

				MPI_Reduce(&readAmount, &sum, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD);

				// Free temporary buffers
				for (var=0 ; var<currentBatchSize; var++)  
					freeVar(var);

				// Time for the current batch
				if (myrank == root) {
					totalTime += max;
				}
		}

		/* Bytes read */
		if (retval = ncmpi_inq_get_size (ncid_g, &getSize)) ERR(retval);
		readAmount = (double)getSize/oneMB;
		MPI_Reduce(&readAmount, &sum, 1, MPI_DOUBLE, MPI_SUM, root, MPI_COMM_WORLD);

		/* Deallocate memory */
		free(varid);
		free(request);
		free(status);

#ifdef BGQ
		getMemStats(1);
#endif

		/* Close the file */
		tStart = MPI_Wtime();
		if (retval = ncmpi_close(ncid_g)) ERR(retval);
		tEnd = MPI_Wtime() - tStart;
		MPI_Reduce(&tEnd, &max, 1, MPI_DOUBLE, MPI_MAX, root, MPI_COMM_WORLD);
		closeT = max;

		/* Clean up */
		MPI_Finalize();

		/*Dump statistics*/

		if (myrank == root) {
				printf("\n%s: %d %d %d %d %d %d %d %7.5lf %7.5lf %7.5lf %7.5lf sec\n", argv[0], commsize, nodenum, ppn, NONBLOCKING, BATCHSIZE, VERSION, COLL, sum, openT, closeT, totalTime);
		}

		return 0;

}

