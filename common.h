#ifndef __common__
#define __common__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mpi.h>
#include <string.h>
#include <math.h>
#include <pnetcdf.h>

#include "mpaspect.h"

#ifdef BGQ
#include <spi/include/kernel/memory.h>
#include <mpix.h>
MPIX_Hardware_t hw;
#endif

//Variable definitions

//command line arguments
extern uint8_t NONBLOCKING, BATCHSIZE, VERSION, COLL;

extern double oneMB;

// Boundary data file 
extern const char *filename;
// List-of-variables file 
extern const char *VAR_FNAME;

//Max length of variable name
extern int VARNAMESIZE;

//Total number of variables to be read
extern int NUMVARS;//=128;
//int NUMVARS=136;

//Number of X and Yvariables
extern uint8_t NUMVARS_X, NUMVARS_Y;

// This will be the netCDF ID for the file and data variable.
extern int ncid_g;

//NC File stats - number of dimensions, number of variables
extern int ndims; 		//Number of variable dimensions
extern int nvarsp;		//Number of variables

//Variable buffers
extern char **fields;												//Variable list
extern float **dataIn; 											//Variable data buffer
extern int *ncid;														//List of NetCDF IDs
extern int *varid;														//List of variable IDs
extern char *memOrder;												//List of memory orders
extern int *size;														//List of variable sizes 
extern MPI_Offset **start, **count, *dimlen; //List of starts, counts and length of dimensions
extern int *rcIndex; 												//Row column index of variable

//mget-related
//Variable data buffer 
extern float **datasets;
extern MPI_Offset *bufcounts;
extern MPI_Datatype *datatypes;

//MPI related
extern int ppn, core, nodenum;
extern int myrank, commsize;
extern int px, py, procs;
extern int root;	//Root process
extern int *request, *status;

//Timers
extern double max, t1, t2, t3, t4; 
extern double tStart, tEnd, tRead, tScatter, tBcast, tDist;
extern double maxDist, maxBcast, maxScatter;
extern double openT, closeT, scatterT, bcastT;
extern double totalTimeRead, totalTimeDist, totalTime;

//Bytes read
extern MPI_Offset getSize, getSizeX, getSizeY;
extern double totalReadSize, readAmount, sum;

extern int currentBatchSize, numBatches;

#ifdef BGQ
extern uint64_t heapmax[2], estheapmax[2], stack[2], stackavail[2], heap[2], heapavail[2];
#endif

//Temporary variables
//int x, y; 
extern int retval, procs;
//End of Variable definitions

/* 
 * Handle errors by printing an error message and exiting with a non-zero status. 
 */
#define ERRCODE 2
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE);}

#endif
