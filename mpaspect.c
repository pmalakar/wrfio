/*
 *  Adapted from WRF code
 *  Input: Number of processes P
 *  Output: Square-like decomposition of P
 */

#include "mpaspect.h"

int mpaspect(int P) {
	
	int M, N, MINI, MINM, MINN, PROCMIN_M=1, PROCMIN_N=1;

	MINI = 2*P;
	MINM = 1;
	MINN = P;
	for (M=1; M<=P ; M++) {
		if (P%M == 0) {
			N = P / M;
			if ( abs(M-N) < MINI && M >= PROCMIN_M && N >= PROCMIN_N ) {
				MINI = abs(M-N);
				MINM = M;
				MINN = N;
			}
		}
	}

	if ( MINM < PROCMIN_M || MINN < PROCMIN_N ) 
			printf("MPASPECT: UNABLE TO GENERATE PROCESSOR MESH.  STOPPING.\n");
	
	return MINM;
}

