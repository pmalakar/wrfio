/*
 *  Common utils
 */

#include "common.h"

//Variable definitions

//command line arguments
uint8_t NONBLOCKING, BATCHSIZE, VERSION, COLL;

double oneMB=1048576.0;

// Boundary data file 
const char *filename = "wrfbdy_d01";

// List-of-variables file 
const char *VAR_FNAME = "vars.txt";

//Max length of variable name
int VARNAMESIZE=20;

//Total number of variables to be read
int NUMVARS;

//Number of X and Yvariables
uint8_t NUMVARS_X=0, NUMVARS_Y=0;

// This will be the netCDF ID for the file and data variable.
int ncid_g;

//NC File stats - number of dimensions, number of variables
int ndims; 		//Number of variable dimensions
int nvarsp;		//Number of variables

//Variable buffers
char **fields;												//Variable list
float **dataIn; 											//Variable data buffer
int *ncid;														//List of NetCDF IDs
int *varid;														//List of variable IDs
char *memOrder;												//List of memory orders
int *size;														//List of variable sizes 
MPI_Offset **start, **count, *dimlen; //List of starts, counts and length of dimensions
int *rcIndex; 												//Row column index of variable

//mget-related
//Variable data buffer 
float **data;
float **datasets;
MPI_Offset *bufcounts;
MPI_Datatype *datatypes;

//MPI related
int ppn=1, core=1, nodenum=1;
int myrank, commsize;
int px, py, procs;
int root = 0;	//Root process
int *request, *status;

//Timers
double max, t1, t2, t3, t4; 
double tStart, tEnd, tRead, tScatter, tBcast, tDist;
double maxDist, maxBcast, maxScatter;
double openT = 0.0, closeT = 0.0, scatterT = 0.0, bcastT = 0.0;
double totalTimeRead = 0.0, totalTimeDist = 0.0, totalTime = 0.0;

//Bytes read
MPI_Offset getSize=0, getSizeX=0, getSizeY=0;
double totalReadSize=0.0, readAmount = 0.0, sum=0.0;

int currentBatchSize, numBatches;

#ifdef BGQ
uint64_t heapmax[2], estheapmax[2], stack[2], stackavail[2], heap[2], heapavail[2];
#endif

//Temporary variables
//int x, y; 
int retval, procs;
//End of Variable definitions


